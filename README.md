# Il DNS Gigante
Fast subdomain brute-forcer written in Rust (work in progress). The goal is to maintain both high speed and accuracy by distributing DNS requests over multiple nameservers, and adjusting transmission rate so as not to congest the network.
