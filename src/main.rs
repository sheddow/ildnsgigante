#![feature(test)]
#![feature(drain_filter)]
use std::cmp::max;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::net::Ipv4Addr;
use std::time::Duration;

mod dns;
mod resolver;
use resolver::resolve_domains;

use clap::{App, Arg};

const QPS_PER_RESOLVER: u32 = 100;
const DEFAULT_MAX_BANDWIDTH: u32 = 1_000_000; // bytes per second
const AVG_BYTES_PER_PACKET: u32 = 100;

// TODO: validate nameservers

fn main() -> Result<(), Box<std::error::Error>> {
    let matches = App::new("Il DNS Gigante")
        .version("1.0")
        .author("Sigurd Kolltveit <sigurd.kolltveit@gmx.com>")
        .about("The fastest DNS resolver ever")
        .arg(
            Arg::with_name("nameservers")
                .short("n")
                .long("nameservers")
                .value_name("FILE")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("domains")
                .help("File containing domains to resolve")
                .required(true),
        )
        .arg(
            Arg::with_name("bandwidth")
                .help("Max allowed bandwidth")
                .short("b")
                .long("bandwidth")
                .takes_value(true),
        )
        .get_matches();

    let ns_file = File::open(matches.value_of("nameservers").unwrap())?;
    let nameservers: Vec<Ipv4Addr> = BufReader::new(ns_file)
        .lines()
        .map(|x| x.unwrap().parse())
        .collect::<Result<_, _>>()
        .map_err(|e| format!("unable to parse nameservers: {}", e))?;

    let domain_file = File::open(matches.value_of("domains").unwrap())?;
    let domains_iter = BufReader::new(domain_file).lines().map(|x| x.unwrap());

    let max_bandwidth = if let Some(bandwidth_arg) = matches.value_of("bandwidth") {
        let mut bandwidth = bandwidth_arg.to_string();
        let multiplier = match bandwidth_arg.chars().last() {
            Some('M') => {
                bandwidth.pop();
                1_000_000
            }
            Some('K') => {
                bandwidth.pop();
                1_000
            }
            _ => 1,
        };

        bandwidth.parse::<u32>()? * multiplier
    } else {
        DEFAULT_MAX_BANDWIDTH
    };

    let bandwidth_limited = Duration::from_secs(1) / (max_bandwidth / AVG_BYTES_PER_PACKET);
    let resolver_limited = (Duration::from_secs(1) / QPS_PER_RESOLVER) / (nameservers.len() as u32);

    eprintln!("Bandwidth allows packet every {:?}", bandwidth_limited);
    eprintln!("Resolvers allow packet every {:?}", resolver_limited);

    let period = max(bandwidth_limited, resolver_limited);
    eprintln!("Sending query every {:?}", period);

    resolve_domains(domains_iter, nameservers, period)?;

    Ok(())
}
