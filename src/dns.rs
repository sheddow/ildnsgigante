extern crate byteorder;
extern crate test;

use std::error;
use std::fmt;
use std::io;
use std::io::Cursor;
use std::io::Read;
use std::net::Ipv4Addr;
use std::net::SocketAddr;
use std::net::UdpSocket;
use std::str;

use self::byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};

#[derive(Debug)]
pub enum DNSError {
    IOError(io::Error),
    UTF8Error(str::Utf8Error),
    ParseError(String),
}

impl From<io::Error> for DNSError {
    fn from(error: io::Error) -> Self {
        DNSError::IOError(error)
    }
}

impl From<str::Utf8Error> for DNSError {
    fn from(error: str::Utf8Error) -> Self {
        DNSError::UTF8Error(error)
    }
}

impl From<&str> for DNSError {
    fn from(error: &str) -> Self {
        DNSError::ParseError(error.to_string())
    }
}

impl error::Error for DNSError {}

impl fmt::Display for DNSError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug, PartialEq)]
pub enum RData {
    A(Vec<Ipv4Addr>),
    CNAME(String),
    NXDOMAIN,
}

#[derive(Debug)]
pub enum ServerError {
    FORMERR,
    SERVFAIL,
    NOTIMP,
    REFUSED,
}

#[derive(Debug)]
pub enum DNSResponse {
    Success { name: String, data: RData },
    Failure(ServerError),
}

fn read_labels(bytes: &[u8], offset: u64) -> Result<(String, u64), DNSError> {
    let mut cursor = Cursor::new(bytes);
    cursor.set_position(offset);

    let mut saved_position = None;

    let mut label_buf = [0u8; 64];
    let mut qname = String::with_capacity(128);
    loop {
        let x = cursor.read_u8()?;
        if x & 0xc0 == 0xc0 {
            // pointer
            if saved_position.is_some() {
                return Err("Multiple pointers in a label sequence is unsupported".into());
            }
            let pointer = ((x & !0xc0u8) as u64) * 256 + cursor.read_u8()? as u64;
            saved_position = Some(cursor.position());
            cursor.set_position(pointer); // Read the rest of the labels from pointer
        } else if x & 0xc0 == 0 {
            let label_length = x;
            if label_length == 0 {
                break;
            }
            cursor.read_exact(&mut label_buf[..(label_length as usize)])?;
            let label = str::from_utf8(&label_buf[..(label_length as usize)])?;
            qname.push_str(&label);
            qname.push('.');
        } else {
            return Err("Invalid pointer/label length".into());
        }
    }
    qname.pop(); // remove final dot

    Ok((qname, saved_position.unwrap_or(cursor.position())))
}

// https://www.ietf.org/rfc/rfc1035.txt
impl DNSResponse {
    pub fn from_socket(socket: &UdpSocket) -> Result<(Self, SocketAddr), DNSError> {
        let mut buf = [0; 512];
        let (_, src_addr) = socket.recv_from(&mut buf)?;
        DNSResponse::from_bytes(&buf).map(|r| (r, src_addr))
    }

    pub fn from_bytes(bytes: &[u8]) -> Result<Self, DNSError> {
        let mut cursor = Cursor::new(bytes);

        // Header section
        let _id = cursor.read_u16::<BigEndian>()?;
        let flags = cursor.read_u16::<BigEndian>()?;

        let rcode = flags & 0xf;

        match rcode {
            1 => return Ok(DNSResponse::Failure(ServerError::FORMERR)),
            2 => return Ok(DNSResponse::Failure(ServerError::SERVFAIL)),
            4 => return Ok(DNSResponse::Failure(ServerError::NOTIMP)),
            5 => return Ok(DNSResponse::Failure(ServerError::REFUSED)),
            _ => {}
        }

        if flags & 0x8000 == 0 {
            return Err("this is a query...".into());
        }

        if flags & 0x200 != 0 {
            return Err("truncated responses are currently not supported".into());
        }

        let qdcount = cursor.read_u16::<BigEndian>()?;
        let ancount = cursor.read_u16::<BigEndian>()?;
        let _nscount = cursor.read_u16::<BigEndian>()?;
        let _arcount = cursor.read_u16::<BigEndian>()?;

        if qdcount != 1 {
            return Err("Question count should be 1".into());
        }

        // Question section

        let (qname, new_pos) = read_labels(&bytes, cursor.position())?;
        cursor.set_position(new_pos);

        let qtype = cursor.read_u16::<BigEndian>()?;
        let qclass = cursor.read_u16::<BigEndian>()?;

        if qtype != 1 {
            return Err("Query type should be A".into());
        }
        if qclass != 1 {
            return Err("Query class should be IN".into());
        }

        if rcode == 3 {
            return Ok(DNSResponse::Success {
                name: qname,
                data: RData::NXDOMAIN,
            });
        }

        // Answer section

        let mut ips = vec![];

        for _ in 0..ancount {
            let (name, new_pos) = read_labels(&bytes, cursor.position())?;
            cursor.set_position(new_pos);

            assert_eq!(name, qname);

            let qtype = cursor.read_u16::<BigEndian>()?;
            let _qclass = cursor.read_u16::<BigEndian>()?;
            let _ttl = cursor.read_u32::<BigEndian>()?;
            let rdlength = cursor.read_u16::<BigEndian>()?;

            if qtype == 1 {
                // A
                // Gather all IPs for A records
                if rdlength != 4 {
                    return Err("Length of A records should be 4".into());
                }
                let mut rdata = [0; 4];
                cursor.read_exact(&mut rdata)?;
                ips.push(Ipv4Addr::from(rdata));
            } else if qtype == 5 {
                // CNAME
                // Exit early for CNAME records
                let (cname, _) = read_labels(&bytes, cursor.position())?;
                return Ok(DNSResponse::Success {
                    name: qname,
                    data: RData::CNAME(cname),
                });
            } else {
                return Err("Unrecognized qtype".into());
            }
        }

        ips.sort();

        Ok(DNSResponse::Success {
            name: qname,
            data: RData::A(ips),
        })
    }
}

pub fn create_dns_request(id: u16, name: &str) -> Vec<u8> {
    let mut request: Vec<u8> = vec![];

    // Header
    // Id
    request.write_u16::<BigEndian>(id).unwrap();
    // Flags, qdcount, ancount, nscount, arcount
    request.extend_from_slice(&[0x01, 0x00, 0, 1, 0, 0, 0, 0, 0, 0]);

    // Query section
    // qname
    let labels = name.split('.');
    for label in labels {
        request.push(label.len() as u8);
        request.extend_from_slice(label.as_bytes());
    }
    request.push(0);
    // qtype (A), qclass (IN)
    request.extend_from_slice(&[0, 1, 0, 1]);

    request
}

#[cfg(test)]
mod tests {
    use self::test::Bencher;
    use super::*;

    #[test]
    fn test_dns_parsing_a() {
        let bytes = vec![
            0x30, 0x39, 129, 128, 0, 1, 0, 1, 0, 0, 0, 0, 3, 110, 114, 107, 2, 110, 111, 0, 0, 1,
            0, 1, 192, 12, 0, 1, 0, 1, 0, 0, 0x30, 0x39, 0, 4, 127, 0, 0, 1,
        ];
        let resp = DNSResponse::from_bytes(&bytes).unwrap();
        if let DNSResponse::Success { name, data } = resp {
            assert_eq!(name, "nrk.no");
            assert_eq!(data, RData::A(vec!["127.0.0.1".parse().unwrap()]));
        } else {
            panic!("Unexpected failure");
        }
    }

    #[test]
    fn test_dns_parsing_cname() {
        let bytes = vec![
            6, 112, 129, 128, 0, 1, 0, 3, 0, 0, 0, 1, 4, 116, 101, 115, 116, 2, 115, 51, 9, 97,
            109, 97, 122, 111, 110, 97, 119, 115, 3, 99, 111, 109, 0, 0, 1, 0, 1, 192, 12, 0, 5, 0,
            1, 0, 0, 29, 95, 0, 19, 16, 115, 51, 45, 100, 105, 114, 101, 99, 116, 105, 111, 110,
            97, 108, 45, 119, 192, 20, 192, 51, 0, 5, 0, 1, 0, 0, 21, 169, 0, 9, 6, 115, 51, 45,
            49, 45, 119, 192, 20, 192, 82, 0, 1, 0, 1, 0, 0, 0, 1, 0, 4, 52, 216, 186, 59, 0, 0,
            41, 16, 0, 0, 0, 0, 0, 0, 0,
        ];
        let resp = DNSResponse::from_bytes(&bytes).unwrap();
        if let DNSResponse::Success { name, data } = resp {
            assert_eq!(name, "test.s3.amazonaws.com");
            assert_eq!(
                data,
                RData::CNAME("s3-directional-w.amazonaws.com".to_string())
            );
        } else {
            panic!("Unexpected failure");
        }
    }

    #[test]
    fn test_dns_parsing_empty_resp() {
        let bytes = vec![
            150, 233, 129, 131, 0, 1, 0, 0, 0, 1, 0, 0, 11, 100, 111, 101, 115, 110, 116, 101, 120,
            105, 115, 116, 7, 101, 120, 97, 109, 112, 108, 101, 3, 99, 111, 109, 0, 0, 1, 0, 1,
            192, 24, 0, 6, 0, 1, 0, 0, 7, 255, 0, 45, 3, 115, 110, 115, 3, 100, 110, 115, 5, 105,
            99, 97, 110, 110, 3, 111, 114, 103, 0, 3, 110, 111, 99, 192, 57, 120, 88, 31, 4, 0, 0,
            28, 32, 0, 0, 14, 16, 0, 18, 117, 0, 0, 0, 14, 16,
        ];
        let resp = DNSResponse::from_bytes(&bytes).unwrap();
        if let DNSResponse::Success { name, data } = resp {
            assert_eq!(name, "doesntexist.example.com");
            assert_eq!(data, RData::NXDOMAIN);
        } else {
            panic!("Unexpected failure");
        }
    }

    #[bench]
    fn bench_parse_cname_response(b: &mut Bencher) {
        let bytes = vec![
            6, 112, 129, 128, 0, 1, 0, 3, 0, 0, 0, 1, 4, 116, 101, 115, 116, 2, 115, 51, 9, 97,
            109, 97, 122, 111, 110, 97, 119, 115, 3, 99, 111, 109, 0, 0, 1, 0, 1, 192, 12, 0, 5, 0,
            1, 0, 0, 29, 95, 0, 19, 16, 115, 51, 45, 100, 105, 114, 101, 99, 116, 105, 111, 110,
            97, 108, 45, 119, 192, 20, 192, 51, 0, 5, 0, 1, 0, 0, 21, 169, 0, 9, 6, 115, 51, 45,
            49, 45, 119, 192, 20, 192, 82, 0, 1, 0, 1, 0, 0, 0, 1, 0, 4, 52, 216, 186, 59, 0, 0,
            41, 16, 0, 0, 0, 0, 0, 0, 0,
        ];
        b.iter(|| DNSResponse::from_bytes(&bytes));
    }

    #[bench]
    fn bench_parse_empty_response(b: &mut Bencher) {
        let bytes = vec![
            150, 233, 129, 131, 0, 1, 0, 0, 0, 1, 0, 0, 11, 100, 111, 101, 115, 110, 116, 101, 120,
            105, 115, 116, 7, 101, 120, 97, 109, 112, 108, 101, 3, 99, 111, 109, 0, 0, 1, 0, 1,
            192, 24, 0, 6, 0, 1, 0, 0, 7, 255, 0, 45, 3, 115, 110, 115, 3, 100, 110, 115, 5, 105,
            99, 97, 110, 110, 3, 111, 114, 103, 0, 3, 110, 111, 99, 192, 57, 120, 88, 31, 4, 0, 0,
            28, 32, 0, 0, 14, 16, 0, 18, 117, 0, 0, 0, 14, 16,
        ];
        b.iter(|| DNSResponse::from_bytes(&bytes));
    }

    #[bench]
    fn bench_parse_a_response(b: &mut Bencher) {
        let bytes = vec![
            0x30, 0x39, 129, 128, 0, 1, 0, 1, 0, 0, 0, 0, 3, 110, 114, 107, 2, 110, 111, 0, 0, 1,
            0, 1, 192, 12, 0, 1, 0, 1, 0, 0, 0x30, 0x39, 0, 4, 127, 0, 0, 1,
        ];
        b.iter(|| DNSResponse::from_bytes(&bytes));
    }

    #[bench]
    fn bench_create_dns_request(b: &mut Bencher) {
        b.iter(|| create_dns_request(12345, "nrk.no"));
    }
}
