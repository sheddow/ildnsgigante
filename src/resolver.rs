use std::collections::{HashSet, HashMap};
use std::cmp::{min, max};
use std::io;
use std::net::{SocketAddr, Ipv4Addr, UdpSocket};
use std::sync::mpsc::channel;
use std::sync::mpsc::Receiver;
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;
use std::time::{Duration, Instant};

use crate::dns::create_dns_request;
use crate::dns::DNSError;
use crate::dns::DNSResponse;
use crate::dns::RData;

use itertools::Itertools;

use lazy_static::lazy_static;

use regex::Regex;

use rand::Rng;
use rand::FromEntropy;
use rand::rngs::SmallRng;



const WINDOW_SIZE: usize = 1_000_000;
const RETRY_TIMEOUT: Duration = Duration::from_millis(200);
const SOCKET_TIMEOUT: Duration = Duration::from_millis(5);

pub fn resolve_domains<I>(
    domains: I,
    nameservers: Vec<Ipv4Addr>,
    period: Duration,
) -> Result<(), Box<std::error::Error>>
where
    I: IntoIterator<Item = String>,
{
    let chunks_iter = domains.into_iter().chunks(WINDOW_SIZE);
    let mut chunked_domains = chunks_iter.into_iter();

    let mut invalid_domains = Vec::new();

    let mut retries = RetryScheduler::new(period, RETRY_TIMEOUT);
    let ns_ratings = Arc::new(NSRatings::new(nameservers.iter().cloned()));

    let socket = Arc::new(UdpSocket::bind(("0.0.0.0", 0))?);
    socket.set_read_timeout(Some(SOCKET_TIMEOUT))?;
    let (domain_sender, domain_receiver) = channel();

    {
        let ns_ratings = Arc::clone(&ns_ratings);
        let socket = Arc::clone(&socket);
        thread::spawn(move || send_dns_requests(&socket, domain_receiver, nameservers, &ns_ratings, period));
    }

    let mut is_more_domains = true;
    while is_more_domains || !retries.is_empty() {
        if let Some(next_chunk) = chunked_domains.next() {
            let mut domains: Vec<String> = next_chunk.collect();
            invalid_domains.extend(domains.drain_filter(|d| !is_valid_domain(d)));

            retries.schedule(domains.iter().cloned());

            domain_sender.send(domains)?;
        } else {
            is_more_domains = false;
        }

        loop {
            let res = DNSResponse::from_socket(&socket);

            match res {
                Ok((DNSResponse::Success { name, data }, src_addr)) => {
                    ns_ratings.increase_rating(src_addr, 10);
                    retries.remove_domain(&name);
                    if data != RData::NXDOMAIN {
                        println!("{} -> {:?}", name, data);
                    }
                }
                Ok((DNSResponse::Failure(e), src_addr)) => {
                    eprintln!("Received {:?} from {}", e, src_addr.ip());
                    ns_ratings.clear_rating(src_addr);
                }
                Err(DNSError::IOError(ref e))
                    if e.kind() == io::ErrorKind::WouldBlock
                        || e.kind() == io::ErrorKind::TimedOut =>
                {
                    break;
                }
                Err(DNSError::ParseError(e)) => {
                    eprintln!("{}", e);
                }
                Err(e) => {
                    panic!("{}", e);
                }
            }
        }

        retries.clear_empty();

        let retry_domains = retries.get_expired();
        if !retry_domains.is_empty() {
            eprintln!("Missing {} responses. Retrying...", retry_domains.len());

            retries.schedule(retry_domains.iter().cloned());

            domain_sender.send(retry_domains)?;
        }
    }

    if !invalid_domains.is_empty() {
        eprintln!("Dropped invalid domains: {:?}", invalid_domains);
    }

    eprintln!("Retransmitted {} requests in total", retries.total_retries);

    Ok(())
}

fn send_dns_requests(
    socket: &UdpSocket,
    recv_domains: Receiver<Vec<String>>,
    nameservers: Vec<Ipv4Addr>,
    ns_ratings: &NSRatings,
    period: Duration,
) {

    let mut nameserver_iter = nameservers
        .iter()
        .cycle()
        .filter(|&ns| ns_ratings.filter(*ns));
    let mut prev_time = Instant::now();
    while let Ok(domains) = recv_domains.recv() {
        for domain in domains.iter() {
            let ns = nameserver_iter.next().unwrap();
            let dns_req = create_dns_request(1, &domain);
            socket.send_to(&dns_req, (*ns, 53)).unwrap();

            let elapsed = prev_time.elapsed();
            let to_sleep = period.checked_sub(elapsed).unwrap_or(Duration::new(0, 0));
            thread::sleep(to_sleep);

            prev_time = Instant::now();
        }
    }
}


// base_penalty increases over time to penalize unresponsive resolvers, but should be offset
// by ratings that increases over time for responsive resolvers
struct NSRatings {
    ratings: Mutex<HashMap<Ipv4Addr, u32>>,
    current_max: AtomicU32,
}

impl NSRatings {
    fn new<I: IntoIterator<Item = Ipv4Addr>>(nameservers: I) -> Self {
        NSRatings {
            ratings: Mutex::new(nameservers.into_iter().map(|x| (x, 100)).collect()),
            current_max: AtomicU32::new(100),
        }
    }

    fn filter(&self, ns: Ipv4Addr) -> bool {
        let mut rng = SmallRng::from_entropy();

        let ratings = self.ratings.lock().unwrap();
        let rating = ratings.get(&ns).unwrap();
        let adjusted = ((*rating as f32)*1.5) as u32;

        let current_max = self.current_max.load(Ordering::Relaxed);

        rng.gen_ratio(min(adjusted, current_max), current_max)
    }

    fn increase_rating(&self, ns: SocketAddr, rating_inc: i32) {
        if let SocketAddr::V4(addr) = ns {
            self.ratings.lock().unwrap().entry(*addr.ip()).and_modify(|x| {
                let new_max = max(*x, self.current_max.load(Ordering::Relaxed));
                self.current_max.store(new_max, Ordering::Relaxed);
                *x = ((*x as i32) + rating_inc) as u32;
            });
        }
    }

    fn clear_rating(&self, ns: SocketAddr) {
        if let SocketAddr::V4(addr) = ns {
            self.ratings.lock().unwrap().entry(*addr.ip()).and_modify(|x| *x = 0);
        }
    }
}


fn is_valid_domain(domain: &str) -> bool {
    lazy_static! {
        static ref DOMAIN_REGEX: Regex =
            Regex::new(r"^([a-zA-Z0-9-]{1,63})(\.[a-zA-Z0-9-]{1,63})+$").unwrap();
    }

    domain.len() < 256 && DOMAIN_REGEX.is_match(domain)
}

struct RetryScheduler {
    period: Duration,
    timeout: Duration,
    buf: Vec<(Instant, HashSet<String>)>,
    total_retries: usize,
}

impl RetryScheduler {
    fn new(period: Duration, timeout: Duration) -> Self {
        RetryScheduler {
            period,
            timeout,
            buf: Vec::new(),
            total_retries: 0,
        }
    }

    fn schedule<I: IntoIterator<Item = String>>(&mut self, domains: I) {
        let set: HashSet<String> = domains.into_iter().collect();
        let retry_at = Instant::now() + (set.len() as u32) * self.period + self.timeout;
        self.buf.push((retry_at, set));
    }

    fn remove_domain(&mut self, domain: &str) {
        for (_, domains) in self.buf.iter_mut() {
            domains.remove(domain);
        }
    }

    fn get_expired(&mut self) -> Vec<String> {
        let expired = self.buf
            .drain_filter(|(retry_at, _)| *retry_at < Instant::now())
            .map(|x| x.1)
            .flatten()
            .collect::<Vec<String>>();
        self.total_retries += expired.len();
        expired
    }

    fn clear_empty(&mut self) {
        self.buf.retain(|(_, domains)| !domains.is_empty());
    }

    fn is_empty(&self) -> bool {
        self.buf.is_empty()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_retry_scheduler_empty() {
        let mut retries = RetryScheduler::new(Duration::from_secs(1), RETRY_TIMEOUT);
        retries.schedule(vec!["example.com".to_string()]);

        let res = retries.get_expired();
        assert_eq!(res, Vec::<String>::new())
    }

    #[test]
    fn test_retry_scheduler() {
        let mut retries = RetryScheduler::new(Duration::new(0, 0), Duration::new(0, 0));
        retries.schedule(vec!["google.com".to_string(), "example.com".to_string()]);

        retries.remove_domain("google.com");

        let res = retries.get_expired();
        assert_eq!(res, vec!["example.com".to_string()])
    }
}
